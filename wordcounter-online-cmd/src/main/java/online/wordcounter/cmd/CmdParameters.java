package online.wordcounter.cmd;

import com.beust.jcommander.Parameter;
import java.util.ArrayList;
import java.util.List;

public class CmdParameters {
    
    @Parameter(description = "URLs")
    public List<String> urls = new ArrayList<>();
    
    @Parameter(names = { "-s", "--site-map" }, description = "Sitemap")
    public String sitemapUrl;
 
    @Parameter(names = { "-f", "--file" }, description = "File with URLs")
    public String fileUrl;
    
    @Parameter(
        names = { "-e", "--just-extract-text" },
        description = "Just output the extracted text"
    )
    public boolean justExtractText;

}

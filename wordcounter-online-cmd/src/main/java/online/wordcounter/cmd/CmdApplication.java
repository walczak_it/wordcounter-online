package online.wordcounter.cmd;

import com.beust.jcommander.JCommander;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import online.wordcounter.core.ResourceAnalyser;
import online.wordcounter.core.ResourceStats;
import online.wordcounter.core.SitemapUrlExtractor;

public class CmdApplication {
    
    public static void main(String... args) {
        try {
            JCommander commander = new JCommander();
            CmdParameters parameters = new CmdParameters();
            commander.addObject(parameters);
            commander.parse(args);
            List<String> urls = extractUrls(parameters);
            if (!parameters.justExtractText) {
                analyse(urls);
            } else {
                extractText(urls);
            }
            System.exit(0);
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
    
    private static List<String> extractUrls(CmdParameters parameters) throws IOException, XMLStreamException {
        if (parameters.sitemapUrl != null) {
            return new SitemapUrlExtractor().extract(parameters.sitemapUrl);
        } else if(!parameters.urls.isEmpty()) {
            return parameters.urls;
        } else {
            return Collections.EMPTY_LIST;
        }
    }
    
    private static void analyse(List<String> urls) throws IOException {
        ResourceAnalyser analyser = new ResourceAnalyser();
        for (String url : urls) {
            ResourceStats stats = analyser.analyse("", url);
            System.out.println(stats.toCsvString());
        }
    }

    private static void extractText(List<String> urls) throws IOException {
        ResourceAnalyser analyser = new ResourceAnalyser();
        PrintWriter writer = System.console().writer();
        for (String url : urls) {
            analyser.outputParsingReader(url, writer);
            writer.flush();
        }
    }
}

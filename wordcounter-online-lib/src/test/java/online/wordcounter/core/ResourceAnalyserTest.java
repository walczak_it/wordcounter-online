package online.wordcounter.core;

import java.io.InputStream;
import java.util.Arrays;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class ResourceAnalyserTest {
    
    private static final String RESOURCE_SHEME = "file:/";
    private static final ResourceAnalyser RESOURCE_ANALYSER = new ResourceAnalyser(
        new ParsedToStdoutDecorator()
    );
    
    private final InputStream resourceStream;
    private final String resourceName;
    private final String resourceUrl;
    private final ResourceStats expectedStats;

    public ResourceAnalyserTest(
        String resourcePath,
        int expectedAllCharectersCount,
        int expectedNonWhitespaceCharectersCount,
        int expectedWordsCount
    ) {
        this.resourceStream = ResourceAnalyserTest.class.getResourceAsStream(resourcePath);
        this.resourceName = resourcePath;
        this.resourceUrl = RESOURCE_SHEME + resourcePath;
        this.expectedStats = new ResourceStats(
            resourcePath, resourceUrl,
            expectedAllCharectersCount,
            expectedNonWhitespaceCharectersCount,
            expectedWordsCount
        );
    }
    
    @Test
    public void shouldCountProperly() throws Exception {
        
        // when
        ResourceStats stats = RESOURCE_ANALYSER.analyse(
            resourceName, resourceUrl, resourceStream
        );
        
        // then
        assertThat(stats).isEqualToComparingFieldByField(expectedStats);
    }
    
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(
            new Object[][]{
                {"/simple_file.txt", 67, 54, 10},
                {"/page_simplest.html", 20, 17, 4}
            }
        );
    }
}

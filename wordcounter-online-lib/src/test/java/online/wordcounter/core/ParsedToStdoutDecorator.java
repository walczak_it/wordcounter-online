/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package online.wordcounter.core;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import java.util.function.Function;

public class ParsedToStdoutDecorator implements Function<Reader, Reader> {

    @Override
    public Reader apply(Reader reader) {
        return new FilterReader(reader) {
            
            private boolean firstRead = true;

            @Override
            public int read(char[] cbuf, int off, int len) throws IOException {
                firstReadSeparator();
                int rlen = super.read(cbuf, off, len);
                if (rlen != -1) {
                    System.out.print(Arrays.copyOfRange(cbuf, 0, rlen));
                }
                return rlen;
            }

            @Override
            public int read() throws IOException {
                firstReadSeparator();
                int character = super.read();
                if (character != -1) {
                    System.out.print(character);
                }
                return character;
            }
            
            private void firstReadSeparator() {
                if (firstRead) {
                    firstRead = false;
                    System.out.println();
                    System.out.println("========================================================");
                }
            }
            
        };
    }
}


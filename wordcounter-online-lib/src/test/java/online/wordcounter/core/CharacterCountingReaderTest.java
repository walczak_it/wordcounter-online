package online.wordcounter.core;

import java.io.StringReader;
import java.util.Arrays;
import static org.assertj.core.api.StrictAssertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class CharacterCountingReaderTest {
    
    private final StringReader text;
    private final int expectedAllCharactersCount;
    private final int expectedNonwhitespaceCharactersCount;

    public CharacterCountingReaderTest(
        String text, int expectedAllCharactersCount, int expectedNonwhitespaceCharactersCount
    ) {
        this.text = new StringReader(text);
        this.expectedAllCharactersCount = expectedAllCharactersCount;
        this.expectedNonwhitespaceCharactersCount = expectedNonwhitespaceCharactersCount;
    }
    
    @Test
    @SuppressWarnings("empty-statement")
    public void shouldCountProperlyOnSingleRead() throws Exception {
        
        // given
        CharacterCountingReader countingReader = new CharacterCountingReader(text);
        
        // when
        while (countingReader.read() != -1);
        
        // then
        assertThat(countingReader.getMindfulCharactersCount())
            .describedAs("All letters count")
            .isEqualTo(expectedAllCharactersCount);
        assertThat(countingReader.getNonWhitespaceCharactersCount())
            .describedAs("Letters without whitespaces count")
            .isEqualTo(expectedNonwhitespaceCharactersCount);
    }
    
    @Test
    public void shouldCountProperlyOnBigArrayRead() throws Exception {
        
        // given
        CharacterCountingReader countingReader = new CharacterCountingReader(text);
        char[] cbuf = new char[1024*100];
        
        // when
        countingReader.read(cbuf);
        
        // then
        assertThat(countingReader.getMindfulCharactersCount()).isEqualTo(expectedAllCharactersCount);
        assertThat(countingReader.getNonWhitespaceCharactersCount())
            .isEqualTo(expectedNonwhitespaceCharactersCount);
    }
    
    @Test
    @SuppressWarnings("empty-statement")
    public void shouldCountProperlyOnSmallArrayRead() throws Exception {
        
        // given
        CharacterCountingReader countingReader = new CharacterCountingReader(text);
        char[] cbuf = new char[3];
        
        // when
        while (countingReader.read(cbuf) != -1);
        
        // then
        assertThat(countingReader.getMindfulCharactersCount()).isEqualTo(expectedAllCharactersCount);
        assertThat(countingReader.getNonWhitespaceCharactersCount())
            .isEqualTo(expectedNonwhitespaceCharactersCount);
    }
    
    @Parameterized.Parameters
    public static Iterable<Object[]> data() {
        return Arrays.asList(
            new Object[][]{
                //{"My name is Adam", 15, 12},
               // {"Mam na imię Adam", 16, 13},
                //{"In the next\r\nline", 16, 13},
                //{"Tabs\tand spaces count", 21, 18},                
                {"\t But tailoring ones don't ", 24, 21},
                //{"3425/3 * 5 + (3^2)", 18, 14}
            }
        );
    }
}

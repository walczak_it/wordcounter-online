package online.wordcounter.core;

public class ResourceStats {
    
    private final String resourceName;
    private final String resourceUrl;
    private final int mindfulCharactersCount;
    private final int nonWhitespaceCharactersCount;
    private final int wordsCount;

    public ResourceStats(
        String resourceName, String resourceUrl,
        int mindfulCharactersCount, int nonWhitespaceCharactersCount,
        int wordsCount
    ) {
        this.resourceName = resourceName;
        this.resourceUrl = resourceUrl;
        this.mindfulCharactersCount = mindfulCharactersCount;
        this.nonWhitespaceCharactersCount = nonWhitespaceCharactersCount;
        this.wordsCount = wordsCount;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public int getMindfulCharactersCount() {
        return mindfulCharactersCount;
    }

    public int getNonWhitespaceCharactersCount() {
        return nonWhitespaceCharactersCount;
    }

    public int getWordsCount() {
        return wordsCount;
    }
    
    public String toCsvString() {
        return String.format(
            "%s, %s, %d, %d, %d", resourceName, resourceUrl,
            mindfulCharactersCount, nonWhitespaceCharactersCount, wordsCount
        );
    }
}

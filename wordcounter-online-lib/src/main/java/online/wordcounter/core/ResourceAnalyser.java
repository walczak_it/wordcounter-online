package online.wordcounter.core;

import com.google.common.io.CharStreams;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.function.Function;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.tika.parser.ParsingReader;

public class ResourceAnalyser {
    
    private final Analyzer analyser = new StandardAnalyzer(new CharArraySet(0, false));
    private final Function<Reader, Reader> customParsingReaderDecorator;

    public ResourceAnalyser() {
        this.customParsingReaderDecorator = null;
    }

    public ResourceAnalyser(Function<Reader, Reader> customParsingReaderDecorator) {
        this.customParsingReaderDecorator = customParsingReaderDecorator;
    }
    
    public ResourceStats analyse(String resourceName, String resourceUrl) throws IOException {
        try (InputStream resourceStream = new URL(resourceUrl).openStream()) {
            return analyse(resourceName, resourceUrl, resourceStream);
        }
    }
    
    public void outputParsingReader(String resourceUrl, Appendable output) throws IOException {
        try (InputStream resourceStream = new URL(resourceUrl).openStream()) {
            ParsingReader parsingReader = new ParsingReader(resourceStream);
            CharStreams.copy(parsingReader, output);
        }
    }
    
    protected ResourceStats analyse(
        String resourceName, String resourceUrl, InputStream resourceStream
    ) throws IOException {
        ParsingReader parsingReader = new ParsingReader(resourceStream);
        CharacterCountingReader countingReader = new CharacterCountingReader(parsingReader);
        Reader decorateReader = countingReader;
        if (customParsingReaderDecorator != null) {
            decorateReader = customParsingReaderDecorator.apply(decorateReader);
        }
        int wordsCount = 0;
        try (TokenStream tokenStream = analyser.tokenStream(null, decorateReader)) {
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                ++wordsCount;
            }
        }
        return new ResourceStats(
            resourceName, resourceUrl,
            countingReader.getMindfulCharactersCount(),
            countingReader.getNonWhitespaceCharactersCount(),
            wordsCount
        );
    }
}

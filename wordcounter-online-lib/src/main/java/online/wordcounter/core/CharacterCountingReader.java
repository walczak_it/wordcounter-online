package online.wordcounter.core;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CharacterCountingReader extends FilterReader {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CharacterCountingReader.class);
    
    private int mindfulCharactersCount = 0;
    private int nonWhitespaceCharactersCount = 0;
    private int lastCountedCharacter = -1;

    public CharacterCountingReader(Reader in) {
        super(in);
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int rlen = super.read(cbuf, off, len);
        for (int i = 0; i < rlen; ++i) {
            count(cbuf[i]);
        }
        if (rlen == 0 || rlen < len) {
            count(-1);
        }
        return rlen;
    }

    @Override
    public int read() throws IOException {
        int character = super.read();
        count(character);
        return character;
    }

    public int getMindfulCharactersCount() {
        return mindfulCharactersCount;
    }

    public int getNonWhitespaceCharactersCount() {
        return nonWhitespaceCharactersCount;
    }
    
    private void count(int character) {
        if (isAnySpace(character)) {
            if (
                !isAnySpace(lastCountedCharacter)
                && nonWhitespaceCharactersCount > 0
            ) {
                ++mindfulCharactersCount;
            }
        } else if (isMindful(character)) {
            ++mindfulCharactersCount;
            ++nonWhitespaceCharactersCount;
        } else if(character == -1) { 
            if (isAnySpace(lastCountedCharacter)) {
                --mindfulCharactersCount;
            }
        }
        LOGGER.trace(
            "character='{}', mindful={}, nonWhitespace={}",
            (char) character, mindfulCharactersCount, nonWhitespaceCharactersCount
        );
        lastCountedCharacter = character;
    }
    
    private boolean isAnySpace(int character) {
        return Character.isWhitespace(character) || Character.isSpaceChar(character);
    }
    
    private boolean isMindful(int character) {
        return Character.isValidCodePoint(character)
            && !Character.isISOControl(character)
            && !Character.isSupplementaryCodePoint(character)
            && !Character.isIdentifierIgnorable(character);
    }
}

package online.wordcounter.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class SitemapUrlExtractor {
    
    private static final String LOC_ELEMENT_NAME = "loc";
    
    private final XMLInputFactory inputFactory = XMLInputFactory.newInstance();
    
    public List<String> extract(String sitemapUrl) throws IOException, XMLStreamException {
        InputStream sitemapStream = new URL(sitemapUrl).openStream();
        XMLStreamReader streamReader = inputFactory.createXMLStreamReader(sitemapStream);
        List<String> urls = new ArrayList<>();
        while (streamReader.hasNext()) {
            int event = streamReader.next();
            if (event == XMLStreamReader.START_ELEMENT) {
                String elementName = streamReader.getLocalName();
                if (elementName.equalsIgnoreCase(LOC_ELEMENT_NAME)) {
                    urls.add(streamReader.getElementText());
                }
            }
        }
        return urls;
    }
}

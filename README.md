Text extraction and analisis tool that can batch process online resources.

Uses (Tika)[http://tika.apache.org/] for the text extraction and (Lucene)[https://lucene.apache.org/] for word tokenization.